$(document).ready(function() {
  var view = location.search,
    nowTemp,
    now,
    sinceDate,
    newDate,
    untilDate;


  //CARGA DE VISTAS (TEMPORAL)
  view = view.substring(1);
  $('#pages').load(view + '.html', function() {
    //Calendario//
    $('.datepicker').datepicker();

    $('input, textarea').placeholder();


    //Desabilitar backdrop del modal
    $('.modal').modal({
      backdrop: 'static',
      keyboard: false,
      show: false
    });

    //modal dentro de otro modal
    $('#btnModalShowOther').click(function() {
      $('#mensajeDefocaph').modal('hide');
      $('#mensajeIndeci').modal('show');
    });
    //Deshabilitar F5
    function disableF5(e) {
      if ((e.which || e.keyCode) == 116) e.preventDefault();
    }
    $(document).on("keydown", disableF5);

    //Mostrar modal de alerta voluntarios
    $(".alerta-modal").on("click", function() {
      if ($('.check-all-childrens-1 input').is(':checked')) {
        $('#alerta').modal('show');
        $('#alert_datepicker-until').data('datepicker');
      } else {
        $('#mensajeIndeciSeleccione').modal('show');
      }
    });

    //calendario con rango//
    //Enviar fecha actual
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (day < 10 ? '0' : '') + day + '/' +
      (month < 10 ? '0' : '') + month + '/' +
      d.getFullYear();
    $("#alert_datepicker-since").val(output);

    nowTemp = new Date();
    now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    sinceDate = $('#alert_datepicker-since').datepicker({
      format: 'dd/mm/yyyy',
      startDate: output,
      onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
    }).on("changeDate", function(ev) {
      if (ev.date.valueOf() > untilDate.date.valueOf()) {
        newDate = new Date(ev.date);
        newDate.setDate(newDate.getDate() + 1);
        untilDate.setValue(newDate);
      }
      $('#alert_datepicker-since').datepicker('hide');
    }).data('datepicker');

    untilDate = $('#alert_datepicker-until').datepicker({
      format: 'dd/mm/yyyy',
      onRender: function(date) {
        return date.valueOf() <= sinceDate.date.valueOf() ? 'disabled' : '';
      }
    }).on("changeDate", function(ev) {
      $('#alert_datepicker-until').datepicker('hide');
    }).data('datepicker');

    // Agregar seccion de alumnos //
    var alumnosBlock = 0;
    $(".btn-add-alumnos-a").on("click", function() {
      alumnosBlock++;
      $("#datos-alumnos").before("<div id=alumnos-block-" + alumnosBlock + " class=\"row\"><div class=\"elim-x\"><img src=\"images/slices_borrar.png\"></div><div class=\"elim\"></div><div class=\"col-xs-12 nopadding\"><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Nombres</label><input type=\"text\" class=\"form-control\" placeholder=\"\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Apellidos</label><input type=\"text\" class=\"form-control\" placeholder=\"\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Fecha de nacimiento</label><input type=\"date\" class=\"form-control datepicker\" placeholder=\"\"></div></div><div class=\"col-xs-12 nopadding\"><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label class=\"hiden\">sexo</label><select class=\"select2\"><option>Sexo</option><option>Masculino</option><option>Femenino</option></select></div></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label class=\"hiden\">Grado</label><select class=\"select2\"><option>Grado</option><option>OPCION1</option><option>OPCION2</option><option>OPCION3</option><option>OPCION4</option><option>OPCION5</option></select></div></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Tipo de documento</label><select class=\"select2\"><option></option><option>OPCION1</option><option>OPCION2</option><option>OPCION3</option><option>OPCION4</option><option>OPCION5</option></select></div></div><div class=\"col-xs-12 nopadding\"><div class=\"col-xs-4 nopadding\"></div><div class=\"col-xs-4 nopadding\"></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Numero de documento</label><input type=\"text\" class=\"form-control\" placeholder=\"\"></div></div></div><div class=\"col-xs-12 nopadding\"><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Nombre del padre ó apoderado</label><input type=\"text\" class=\"form-control\"></div></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Tipo de documento</label><select class=\"select2\"><option></option><option>OPCION1</option><option>OPCION2</option><option>OPCION3</option><option>OPCION4</option><option>OPCION5</option></select></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Número de documento</label><input type=\"text\" class=\"form-control\"></div></div></div><div class=\"row\"><div class=\"col-xs-12\"><div class=\"col-xs-4\"></div><div class=\"col-xs-4\"></div><div class=\"col-xs-4\"><div class=\"pull-right\"><span class=\"nombre-archivo\"></span><div class=\"btn btn-modalsmall plomo btn-uploader-container\"><input type=\"file\" class=\"btn-upload-product-picture one-in\"><span class=\"btn-uploader-text\"><em>Adjuntar anexo 3</em></span></div></div></div></div></div>");
      $(".select2").select2({
        minimumResultsForSearch: -1
      });
      $('#alumnos-block-' + alumnosBlock).find('.datepicker').datepicker();
    });

    // Agregar voluntarios participantes //
    var voluntariosBlock = 0;
    $(".btn-add-voluntariosParticipantes").on("click", function() {
      voluntariosBlock++;
      $(".contenido-de-terminos-usuarios").before("<div id=voluntarios-block-" + voluntariosBlock + " class=\"row\"><div class=\"elim-x\"><img src=\"images/slices_borrar.png\"></div><div class=\"elim\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Tipo de documento</label><select class=\"select2\"><option></option><option>Opcion2</option><option>Opcion3</option></select></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Número de documento</label><input type=\"text\" placeholder=\"\" class=\"form-control\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Nombres</label><input type=\"text\" placeholder=\"\" class=\"form-control\"></div></div><div class=\"clearfix\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Apellidos</label><input type=\"text\" placeholder=\"\" class=\"form-control\"></div></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Fecha de nacimiento</label><input type=\"text\" placeholder=\"\" class=\"form-control datepicker\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Correo</label><input type=\"text\" class=\"form-control\" placeholder=\"\"></div></div></div>");
      $(".select2").select2({
        minimumResultsForSearch: -1
      });
      $('#voluntarios-block-' + voluntariosBlock).find('.datepicker').datepicker();
    });

    $(document).on('click', '.btn_aceptarAfiliacion', function() {
      $('.btn_afiliaciones').html('<a class="btn btn-default btn_orange btn_desafiliarme" type="button">DESAFILIARME</a>');
    });

    $(document).on('click', '.btn_desafiliarme', function() {
      $('.btn_afiliaciones').html('<a class="btn btn-default btn_orange btn_aceptarAfiliacion" type="button">ACEPTAR AFILIACIÓN</a><div class="clearfix"></div><a class="btn btn-default btn_orange btn_rechazarAfiliacion" type="button">RECHAZAR AFILIACIÓN</a>');
    });

    $(document).on('click', '.elim-x', function(e) {
      $(this).parent('.row').remove();
    });


    // Actualizar información de grupo //

    $(document).on('click', '.actualizar_info_grupo', function(e) {
      $("tr input:text").removeClass('form-control-s');
      $("tr input:text").prop('disabled', false).first().focus();
      $('.btn_mantenimiento').html('<div class="div_mantenimiento"><a data-toggle="modal" data-target="#agregarParticipante" class="btn-add-cursos-a"><img src="images/slices_mas.png"></a> <button class="btn btn_cancelGestionGrupo">Cancelar</button> <button class="btn btn-saveGestionGrupo">Guardar</button></div>');
    });
    //Save informacion editada //
    $(document).on('click', '.btn-saveGestionGrupo', function(e) {
      $("tr input:text").addClass('form-control-s');
      $('.btn_mantenimiento').html('<a data-toggle="modal" data-target="#agregarParticipante" class="btn-add-cursos-a"><img src="images/slices_mas.png"><span class="btn-add-cursos"><em> Agregar participante</em></span></a>');
    });
    $(document).on('click', '.btn_cancelGestionGrupo', function(e) {
      $("tr input:text").addClass('form-control-s');
      $('.btn_mantenimiento').html('<a data-toggle="modal" data-target="#agregarParticipante" class="btn-add-cursos-a"><img src="images/slices_mas.png"><span class="btn-add-cursos"><em> Agregar participante</em></span></a>');
    });

    $(document).on('click', '.btn_dejarResponsabilidad', function(e) {
      $('.div_elegir').html('<a class="btn_elegir" data-toggle="modal" data-target="#mensajeDejarResponsabilidadGrupo">elegir</a>');
    });

    ///----------------------------------------------------------------------------------------//


    //AGREGAR LISTA DE NOMBRES DE REGISTROS//
    var registerNamesList = [
      [],
      ['Lima', 'Arequipa', 'Cuzco', 'Trujillo', 'Piura', 'Tacna', 'Huancayo', 'Ilo', 'Puno', 'Iquitos', 'Huancavelica', 'Ica', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco', 'Huanuco'],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      ['campo 1', 'campo 2', 'campo 3', 'campo 4', 'campo 5', 'campo 6', 'campo 7', 'campo 8', 'campo 9', 'campo 10', 'campo 11', 'campo 12', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13'],
      [],
      [],
      ['campo 1', 'campo 2', 'campo 3', 'campo 4', 'campo 5', 'campo 6', 'campo 7', 'campo 8', 'campo 9', 'campo 10', 'campo 11', 'campo 12', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13', 'campo 13'],
      []
    ];
    var listaMostrar = [];
    var er;

    $(document).ready(function() {
      for (var  er  =  0;  er  <  registerNamesList[1].length;  er++)  {
        var htmlCiudades = '<li><span>' + registerNamesList[1][er] + '</span><div class="pull-right"><a class="btn_editar_pan"></a><a data-toggle="modal" data-target="#mensajeEliminarRegistro" class="btn_eliminar_pan"></a></div> </li>';
        listaMostrar.push(htmlCiudades);
      }
      listaMostrar = listaMostrar.join('');
      $('.font-black').html(listaMostrar);
      listaMostrar = [];
      $('#13').css('display', 'none');
      $('#10').css('display', 'none');
    });
    //------------------------------------------------------------------------------------------//

    //TABLAS MAESTRAS --------------------------------------------------------------------------//
    $(document).on('click', '.ul_table_maestra li', function() {
      $('.ul_table_maestra li').removeClass("active");
      $(this).addClass('active');
      var arrayId = $(this).attr('data-id');
      var ArrayMostrar = registerNamesList[arrayId];
      console.log(ArrayMostrar);
      for (var  er  =  0;  er  <  ArrayMostrar.length;  er++)  {
        var htmlAlmacenar = '<li><span>' + ArrayMostrar[er] + '</span><div class="pull-right"><a class="btn_editar_pan"></a><a data-toggle="modal" data-target="#mensajeEliminarRegistro" class="btn_eliminar_pan"></a></div> </li>';
        listaMostrar.push(htmlAlmacenar);
      }
      listaMostrar = listaMostrar.join('');
      $('.font-black').html(listaMostrar);
      listaMostrar = [];

      var divmostrar = $(this).attr('data-id');
      $('.input_padding').css('display', 'none');
      $('#' + divmostrar).css('display', 'block');
    });
    //------------------------------------------------------------------------------------------//


    //EDITAR REGISTRO - LISTA DE NOMBRE DE REGISTROS ------------------------------------------//
    $(document).on('click', '.font-black li div > a.btn_editar_pan ', function() {
      var campoEditar = registerNamesList[1][0];

    });

    //-----------------------------------------------------------------------------------------//

    $(".select2").select2({
      placeholder: "Select a state",
      allowClear: true
    });

    $("button").on('click', function() {
      if (!$('.select2 option:selected').is(':selected')) {
        $(".select2").addClass('has_error_select');
      } else {
        $('.select2').removeClass('has_error_select');
      }
    });



    $('#tipo-voluntario').select2().on('change', function() {
      var selectedOption = $(this).val();
      if (selectedOption == 'uno') {
        $('.select-container-first').fadeIn(0);
        $('.select-container').fadeOut(0);
      } else {
        $('.select-container-first').fadeOut(0);
        $('.select-container').fadeIn(0);
      }
    });

    //------------------------------------------------------------------------------------------//
    $(".btn-add-especialidad-a").on("click", function() {
      $("#especialidad").before("<div class=\"row padre\"><div class=\"elim-x-5\"><img src=\"images/slices_borrar.png\"></div><div class=\"elim\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Grado académico</label><select class=\"select2\"><option></option><option>Opcion1</option><option>Opcion2</option><option>Opcion3</option><option>Opcion4</option><option>Opcion5</option></select></div></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Institución</label><select class=\"select2\"><option>UNIVERSIDAD CATOLICA</option><option>IPP</option><option>SAN MARCOS</option><option>IPAD</option><option>IDAD</option><option>OPCION5</option></select></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label class=\"hiden\">Otros</label><input type=\"text\" class=\"form-control\" placeholder=\"otros\"></div></div><div class=\"clearfix\"></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Año en que culmino</label><input type=\"text\" class=\"form-control\" placeholder=\"\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label class=\"hiden\">Adjuntar</label><div><span class=\"nombre-archivo\"></span><div class=\"btn btn-modalsmall plomo btn-uploader-container\"><input type=\"file\" class=\"btn-upload-product-picture one-in\"><span class=\"btn-uploader-text\"><em>Adjuntar certificado de especialización</em></span></div></div></div></div></div>");
      $(".select2").select2({
        minimumResultsForSearch: -1
      });
    });
    $(document).on('click', '.elim-x-5', function(e) {
      $(this).parents('.padre').remove();
    });

    var courseBlock = 0;
    $(".btn-add-cursos-a").on("click", function() {
      courseBlock++;
      $("#cont-info").before("<div id=course-block-" + courseBlock + " class=\"padre\"><div class=\"row\"><div class=\"elim-x-2\"><img src=\"images/slices_borrar.png\"></div><div class=\"elim\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Curso</label><select class=\"select2\"><option></option><option>opcion1</option></select></div></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Institución ( Seleccionar )</label><select class=\"select2\"><option></option><option>opcion1</option></select></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label class=\"hiden\">Otros</label><input type=\"text\" placeholder=\"\" class=\"form-control\"></div></div><div class=\"clearfix\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label class=\"block-label\">Fecha</label><input type=\"text\" class=\"form-control datepicker\" placeholder=\"\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Duración de horas</label><input type=\"text\" class=\"form-control\" placeholder=\"\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label class=\"hiden\">Adjuntar</label><div><span class=\"nombre-archivo\"></span><div class=\"btn btn-modalsmall plomo btn-uploader-container\"><input type=\"file\" class=\"btn-upload-product-picture one-in\"><span class=\"btn-uploader-text\"><em>Adjuntar certificado de capacitación</em></span></div></div></div></div></div></div>");
      $(".select2").select2({
        minimumResultsForSearch: -1
      });
      $('#course-block-' + courseBlock).find('.datepicker').datepicker();
    });
    $(document).on('click', '.elim-x-2', function(e) {
      $(this).parents('.padre').remove();
    });


    $(".btn-add-experiencia-a").on("click", function() {
      $(".div-experiencia").before("<div class=\"div-experiencia1\"><div class=\"row\"><div class=\"elim-x-3\"><img src=\"images/slices_borrar.png\"></div><div class=\"elim\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Institución ( seleccionar )</label><select class=\"select2\"><option></option><option>opcion1</option></select></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label class=\"hiden\">otros</label><input type=\"text\" class=\"form-control\" placeholder=\"Otros\"></div></div><div class=\"clearfix\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Ciudad</label><select class=\"select2 col-xs-2\"><option></option><option>lima</option><option>opcion1</option><option>opcion2</option><option>opcion3</option><option>opcion4</option></select></div></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Cargo</label><select class=\"select2\"><option></option>opcion1<option></option></select></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Tiempo</label><input type=\"text\" class=\"form-control\" placeholder=\"\"></div></div>");
      $(".select2").select2({
        minimumResultsForSearch: -1
      });
    });

    $(document).on('click', '.elim-x-3', function(e) {
      $(this).parents('.div-experiencia1').remove();
    });

    $(document).on('click', '.op-deshabilitar span', function(e) {
      $(this).toggleClass('habilitado');
      $(this).parent().parent("tr").toggleClass('row-grey inactive');
      if ($(this).hasClass('habilitado')) {
        $(this).text('Deshabilitar');
        $(this).parent().parent("tr").find('input[type=checkbox]').iCheck('enable');
        $(this).parent().parent("tr").find(':contains(Inactivo)').text('activo');
      } else {
        $(this).text('Habilitar');
        $(this).parent().parent("tr").find('input[type=checkbox]').iCheck('disable');
        $(this).parent().parent("tr").find(':contains(activo)').text('Inactivo');
      }
    });


    $(".btn-add-actividades-a").on("click", function() {
      $(".div-actividades").before("<div class=\"div-actividades1\"><div class=\"row\"><div class=\"elim-x-4\"><img src=\"images/slices_borrar.png\"></div><div class=\"elim\"></div><div class=\"col-xs-4 nopadding shijo\"><div class=\"form-group winput\"><label>Disciplina deportiva</label><select class=\"select2\"><option></option><option>opcio1</option></select></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Intensidad por semana (en horas)</label><input type=\"text\" placeholder=\"\" class=\"form-control\"></div></div><div class=\"col-xs-4 nopadding\"><div class=\"form-group winput\"><label>Tiempo de práctica (en meses)</label><input type=\"text\" placeholder=\"\" class=\"form-control\"></div></div></div></div>");
      $(".select2").select2({
        minimumResultsForSearch: -1
      });
    });

    $(document).on('click', '.elim-x-4', function(e) {
      $(this).parents('.div-actividades1').remove();
    });


    $('.multiselect').multiselect({
      nonSelectedText: 'Elige una opción',
      allSelectedText: 'Todos'
    });

    $(".select2").select2({
      minimumResultsForSearch: -1
    });

    $('.scrollbox').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle3'
    });

    $('.scrollbox-grupo').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle3'
    });

    $('.scrollbox1').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle3'
    });

    $('.scrollbox2').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle2'
    });

    $('.scrollbox4').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle5'
    });

    $('.scrollbox_panel').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle3'
    });

    $('.icheck-container input').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue',
      increaseArea: '20%'
    });
    //--------------------------------------------------------------------------------
    $(document).on('change', '.btn-uploader-container :file', function() {
      var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    $(document).on('fileselect', '.btn-uploader-container :file', function(event, numFiles, label) {
      $(this).parent().prev('.nombre-archivo').text(label);
    });
    //------------------------------------------------------------------------------------
    $(document).on('change', '.btn-uploader-container-up :file', function() {
      var input = $(this),
        SO,
        archivo = input.val(),
        arr_ruta,
        patron = /(\"|\')/,
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        formato = $(this).attr('data-aceptar'),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      if (SO == "Win") {
        arr_ruta = archivo.split("\\");
      } else {
        arr_ruta = archivo.split("/");
      }
      var nombre_archivo = (arr_ruta[arr_ruta.length - 1]);
      var ext_validas = patron.test(nombre_archivo);
      if (archivo.split(formato).length != 2 || patron.test(archivo)) {
        var htmlmodal = $('body').after('<div class="modal fade" id="mensajeValidacionFormato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"><div class="modal-dialog modal-sm" role="document"><div class="modal-content"><div class="modal-body"><div><p class="text-eliminar text-center"> Formato permitido : ' + formato + '  <br> Su Archivo ' + nombre_archivo + '</p></div></div><div class="modal-footer"><div class="row"><div class="col-xs-12 text-center"><button type="submit" class="btn btn-default btn-enviar" data-dismiss="modal">ACEPTAR</button></div></div></div></div></div></div>');
        $('#mensajeValidacionFormato').modal('show');
        return false;
      }
      input.trigger('fileselect', [numFiles, label, formato]);
    });

    $(document).on('fileselect', '.btn-uploader-container-up :file', function(event, numFiles, label) {
      $(this).parent().find('.nombre-archivo-up').text(label);

    });

    $(document).on('fileselect', '.btn-uploader-container-up :file', function(event, numFiles, label) {
      $('.putFileUp').parent().find('.nombre-archivo-up').text(label);
    });


    //desabilitar check si el estado es inactivo
    $('.data-container').ready(function() {
      if ($('tr:contains(Inactivo)')) {
        $('tr:contains(Inactivo)').addClass('inactive row-grey');
        if ($('tr').hasClass('inactive')) {
          $(".inactive input[type=checkbox]").iCheck('disable');
          $('.inactive').find('.op-deshabilitar').html('<span>Habilitar</span>');
        } else {
          $("input[type=checkbox]").iCheck('enable');
        }
      }
    });


    $('.icheck-container .check-all').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue',
      increaseArea: '20%',
    });

    //SELECCIONAR TODOS LOS CHECKS
    $('.check-all').on('ifChecked', function(event) {
      var dataCheck = $(this).attr('data-check');
      $('.check-all-childrens-' + dataCheck + ' input:enabled').iCheck('check');
    });
    $('.check-all').on('ifUnchecked', function(event) {
      var dataCheck = $(this).attr('data-check');
      $('.check-all-childrens-' + dataCheck + ' input').iCheck('uncheck');
    });
    $('.data-container input[type=checkbox]').on('ifUnchecked', function(event) {
      if ($(this).iCheck('uncheck')) {
        $('.data-container').removeClass('check-all-childrens-1');
        $('.check-all').iCheck('uncheck');
        $('.data-container').addClass('check-all-childrens-1');
      } else if ($('.data-container input:checked').is(":checked")) {
        $('.data-container').addClass('check-all-childrens-1');
      }
    });
    $('.data-container input[type=checkbox]').on('ifChecked', function(event) {
      var inpenabled = $('.data-container input:enabled').size();
      var inpcheck = $('.data-container input:checked').size();
      if( inpenabled == inpcheck){
          $('.check-all').iCheck('check');
      }
    });

  });
});
